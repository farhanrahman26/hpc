#ifndef CORE_INFO
#define CORE_INFO

class CoreInfo {
public:
	
	static void setCores(int numcores){
		CoreInfo::numcores = numcores;
	}


	static int getCores(void){
		return CoreInfo::numcores;
	}

	static int numcores; 

};

int CoreInfo::numcores = -1;

#endif