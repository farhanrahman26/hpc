#ifndef FFT_PROCESS_HPP
#define FFT_PROCESS_HPP

#include <complex>

#include "Process.hpp"

#define EPSILON 0.00001f

class FFTProcess : public Process{
public:
	FFTProcess(
		int n,
		const std::complex<double> *pIn,
		std::complex<double> *pOut
	) : n(n), pIn(pIn), pOut(pOut) {}

	virtual ~FFTProcess(void){} 

	/*Run method to be overidden by different implementations
	 *of the FFT function*/
	virtual void run(void) = 0;
	
	/*Overriding not completed properly*/
	bool compare(const Process &object){
		const FFTProcess *fftProc = dynamic_cast<const FFTProcess *> (&object);
		if (fftProc == NULL) return false;
		if (fftProc->n != n) return false;
		std::complex<double> *oPOut = fftProc->pOut;
		for (int i = 0; i < n; ++i){
			if (fabs(pOut[i].real() - oPOut[i].real()) > EPSILON){				
				return false;
			}
			if (fabs(pOut[i].imag() - oPOut[i].imag()) > EPSILON){
				return false;
			}
		}
		return true;
	}

	int n;
	const std::complex<double> *pIn;
	std::complex<double> *pOut;
};

#endif
