#ifndef FFT_REGULAR_HPP
#define FFT_REGULAR_HPP
#include "FFTProcess.hpp"
#include "fft.hpp"



class FFTRegular : public FFTProcess {
public:
	FFTRegular(
		int n, 
		const std::complex<double> *pIn, 
		std::complex<double> *pOut
	) : FFTProcess(n, pIn, pOut) {}

	void run(void){
		const double pi2=6.283185307179586476925286766559;
		double angle = pi2/n;
		std::complex<double> wn(cos(angle), sin(angle));
		fft_impl(n, wn, pIn, 1, pOut, 1);
	}
};

#endif
