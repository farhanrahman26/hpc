#ifndef GRAPH_DISTANCE_PROCESS
#define GRAPH_DISTANCE_PROCESS
#include "Process.hpp"

class GraphDistanceProcess : public Process {
protected:
	const std::vector<node> *nodes;
	std::vector<int> *distance;
	int start;
public:
	GraphDistanceProcess(
		int start,
		const std::vector<node> *nodes
	) : start(start), nodes(nodes), distance(new std::vector<int>((*nodes).size(), INT_MAX)) {}

	~GraphDistanceProcess(void) {
		delete distance;
	}

	/**
	 *Run method to be overriden by subclasses*/
	virtual void run(void) = 0;

	bool compare(const Process &p){
		const GraphDistanceProcess *gdp = dynamic_cast<const GraphDistanceProcess*>(&p);
		if (gdp == NULL) return false;

		/*Check to see if the start values were the same*/
		if (this->start != gdp->start) return false;

		/*Check to see if the nodes contain the same values*/
		if ((*nodes).size() != (*(*gdp).nodes).size()) return false;

		for(unsigned i = 0; i < (*nodes).size(); ++i) {
			if ((*(*this).nodes)[i].id != (*(*gdp).nodes)[i].id){
				return false;
			}

			unsigned range = (*nodes)[i].edges.size();
			for (unsigned j = 0; j < range; ++j){
				if( (*(*this).nodes)[i].edges[j] != (*(*gdp).nodes)[i].edges[j] )
					return false; 
			}
		}

		/*Compare the distance vector*/
		for(unsigned i = 0; i < (*(*this).distance).size(); ++i){
			if((*(*this).distance)[i] != (*(*gdp).distance)[i]){
				return false;
			}
		}

		/*Return true when everything is equal*/
		return true;
	}

	std::vector<int> getDistance(void){
		return std::vector<int> (*distance);
	}

};



#endif