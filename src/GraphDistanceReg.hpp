#ifndef GRAPH_DISTANCE_REG
#define GRAPH_DISTANCE_REG
#include "GraphDistanceProcess.hpp"
#include "graph_distance.hpp"

class GraphDistanceReg : public GraphDistanceProcess{
public:
	GraphDistanceReg(
		int start,
		const std::vector<node> *nodes
	) : GraphDistanceProcess(start, nodes) {}
	
	void run(void) {
		(*(*this).distance) = graph_distance(*nodes, start);
	}
};

#endif