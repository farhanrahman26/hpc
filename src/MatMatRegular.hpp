#ifndef MAT_MAT_REGULAR_HPP
#define MAT_MAT_REGULAR_HPP
#include <iostream>
#include "MatProcess.hpp"
#include "mat_t.hpp"
#include "mat_mat_mul.hpp"

class MatMatRegular : public MatProcess {
public:
	MatMatRegular(
		mat_t dst,
		const mat_t a,
		const mat_t b
	) : MatProcess(dst,a,b)
	{}

	void run(void){
		mat_mat_mul(dst, a, b);
	}
};


#endif
