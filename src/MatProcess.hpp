#ifndef MAT_PROCESS_H
#define MAT_PROCESS_H

#include "Process.hpp"

#include "mat_t.hpp"

#define EPSILON 0.00001

class MatProcess : public Process {
public:
	MatProcess (
		mat_t dst, 
		const mat_t a, 
		const mat_t b
	) : dst(dst), a(a), b(b) {}

	virtual void run(void) = 0;

	/*Compare the values in the matrix*/
	bool compare(const Process &object) {
		const MatProcess *matProc = dynamic_cast<const MatProcess *> (&object);
		if (matProc == NULL) return false;

		unsigned r = (*matProc).dst.rows;
		unsigned c = (*matProc).dst.cols;

		if ((r != (*this).dst.rows) && (c != (*this).dst.cols))
			return false;

		for (unsigned i = 0; i < r; ++i)
			for (unsigned j = 0; j < c; ++j){
				if (fabs((*this).dst.at(i,j) - matProc->dst.at(i,j)) > EPSILON)
					return false;
				if (fabs((*this).a.at(i,j) - matProc->a.at(i,j)) > EPSILON)
					return false;
				if (fabs((*this).b.at(i,j) - matProc->b.at(i,j)) > EPSILON)
					return false;	
		}

		return true;
	}

protected:
	mat_t dst;
	mat_t a;
	mat_t b;
};

#endif
