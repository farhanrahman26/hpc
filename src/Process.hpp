#ifndef PROCESS_H
#define PROCESS_H

/*Process interface. Convenient for other objects trying to call
 *the run method for any generic process whether it is run parallely,
 *or purely sequentlially*/
class Process {
public:
	/** 
	 *Defines a method that subclasses
	 *can override to run a particular
	 *process.*/
	virtual void run(void) = 0;

	/**
	 *Compare function to be implemented by
	 *derived classes.*/
	virtual bool compare(const Process &object) = 0;

	/*
	 *Virtual destructor added for derived classes
	 *to override*/
	virtual ~Process(void) {};
};

#endif
