#ifndef RUNNER_HPP
#define RUNNER_HPP

#include <vector>
#include <tbb/tick_count.h>
#include <tbb/task_scheduler_init.h>
#include "Process.hpp"

#define ITERATIONS 40
#define DEFAULT_DURATION 5.0f //seconds

class Runner{
public:
	Runner(
		Process *benchMark,
		std::vector<Process *> processes
	) : benchMark(benchMark), processes(processes) {}

	/**Runs the list of processes supplied to the object
	 * at point of instantiation. The output of each process
	 * is tested with the benchmark process do determine
	 * correctness*/
	void runProcesses(void) {
		/*Run the benchmark process to accumulate the result*/
		benchMark->run();
		for (int i = 0; i < processes.size(); ++i) {
			unsigned iter = Runner::determineIterations(processes[i]);
			Runner::runProcess(*processes[i], iter);
			if (!processes[i]->compare(*benchMark)){
				std::cout << "Output error" << std::endl;
			}			
		}
	}

	/**Takes in a process and runs it to determine
	 * the number of iteratinos required to run for
	 * DEFAULT_DURATION seconds.
	 * @param[p] Test process that is run to determine
	 * the number of iterations.*/
	static unsigned determineIterations(Process *p){
		tbb::tick_count start = tbb::tick_count::now();
		p->run();
		tbb::tick_count end = tbb::tick_count::now();
		double delta = (end-start).seconds();
		if (delta > DEFAULT_DURATION) {
			return 1;
		} else if (delta == 0.0) {
			return ITERATIONS;  
		} else {
			unsigned iter = DEFAULT_DURATION / delta;
			return iter == 0 ? ITERATIONS : iter;
		}
		
	}

	/**Runs a process for provided number of iterations. Outputs the
	 * runtime of the particular process once it finishes. 
	 * @param[p] constant reference to an object implementing the functions defined in Process
	 * @param[iter] number of iterations the process will be run before taking measurements */
	static void runProcess(Process &p, unsigned iter = ITERATIONS){
		if (iter == 0) {iter = 1;}
		if(iter == 1){
			tbb::tick_count start = tbb::tick_count::now();
			p.run();
			tbb::tick_count end = tbb::tick_count::now();
			std::cout << (end-start).seconds() << std::endl;
		} else {
			tbb::tick_count start = tbb::tick_count::now();
			for (unsigned i = 0; i < iter; ++i){
				p.run();	
			}
			tbb::tick_count end = tbb::tick_count::now();
			std::cout << ((end-start).seconds() / (double) iter) << std::endl;
		}
	}

private:
	Process *benchMark;
	std::vector<Process *> processes;
};

#endif
