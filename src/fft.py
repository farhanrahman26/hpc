#!/usr/bin/env python

import os
import subprocess

def runFFT():
        proc = [2, 4]
        probsize = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
        for i in range(0, len(proc)):
                for j in range(0, len(probsize)):
                        arg = "./fft_time " + str(probsize[j]) + " " + str(proc[i])
                        os.system(arg)
                print "\n"


if __name__=="__main__":
        runFFT()
