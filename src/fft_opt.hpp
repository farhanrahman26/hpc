#ifndef FFT_OPT
#define FFT_OPT
#include "FFTProcess.hpp"
#include <complex>
#include <tbb/task.h>
#include "fftbasic.hpp"


#define TASK_LIMIT 32768

class FFTOptWorker : public tbb::task {
public:
	/**Constructor for FFTOptWorker
	 * @param[n] problem size
	 * @param[wn] unity
	 * @param[sIn] index pointer for input
	 * @param[sOut] index pointer for output*/	
	FFTOptWorker(
		int n,
		std::complex<double> wn,
		const std::complex<double> *pIn,
		int sIn,
		std::complex<double> *pOut,
		int sOut
	) : n(n), wn(wn), pIn(pIn), sIn(sIn), pOut(pOut), sOut(sOut) {}


	/**Execute function inherited from tbb::task
	 **/
	tbb::task * execute(void){
		if (n == 1){
			pOut[0] = pIn[0];
	    }else if (n == 2){
			pOut[0] = pIn[0]+pIn[sIn];
			pOut[sOut] = pIn[0]-pIn[sIn];
	    }else{
			unsigned m = n>>1;

			/*Agglomeration point. Stop spawning threads when program
			 *hits this point*/
			if (n >= TASK_LIMIT) {
				tbb::task_list tList;
				
				tList.push_back(*new (tbb::task::allocate_child()) FFTOptWorker(m,wn*wn,pIn,2*sIn,pOut,sOut));
				tList.push_back(*new (tbb::task::allocate_child()) FFTOptWorker(m,wn*wn,pIn+sIn,2*sIn,pOut+sOut*m,sOut));
			
				set_ref_count(3);
				tbb::task::spawn_and_wait_for_all(tList);

			}else {
				/*Sequential version of the fft algorithm*/
				fftbasic::fft_seq(m,wn*wn,pIn,2*sIn,pOut,sOut);
				fftbasic::fft_seq(m,wn*wn,pIn+sIn,2*sIn,pOut+sOut*m,sOut);
			}

			std::complex<double> w=std::complex<double>(1.0, 0.0);
			for (unsigned j=0;j<m;++j){
				std::complex<double> t1 = w*pOut[m+j];
				std::complex<double> t2 = pOut[j]-t1;
				pOut[j] = pOut[j]+t1;               /*  pOut[j] = pOut[j] + w^i pOut[m+j] */
				pOut[j+m] = t2;                  	/*  pOut[j] = pOut[j] - w^i pOut[m+j] */
				w = w*wn;
			}
		}	

		return NULL;
	}

private:
	int n;
	std::complex<double> wn;
	const std::complex<double> *pIn;
	int sIn;
	std::complex<double> *pOut;
	int sOut;
};



class FFTOpt : public FFTProcess {
public:
	/**Constructor for the FFTOpt class
	 * @param[n] problem size
	 * @param[pIn] pointer to input data
	 * @param[pOut] pointer to output
	 **/	
	FFTOpt(
		int n,
		const std::complex<double> *pIn,
		std::complex<double> *pOut
	) : FFTProcess(n, pIn, pOut) {}

	/**Run method
	 * Responsible for spawning the root task
	 */
	void run(void){
        const double pi2=6.283185307179586476925286766559;
        double angle = pi2/n;
        std::complex<double> wn(cos(angle), sin(angle));
		FFTOptWorker &fftOptWorker = * new (tbb::task::allocate_root()) FFTOptWorker(n, wn, pIn, 1, pOut, 1);
		tbb::task::spawn_root_and_wait(fftOptWorker);
	}
};

void fft_opt(int n, const std::complex<double> *pIn, std::complex<double> *pOut) {
	FFTOpt fftOpt(n,pIn,pOut);
	fftOpt.run();
}


#endif
