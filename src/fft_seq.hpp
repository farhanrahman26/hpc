#ifndef FFT_SEQ_HPP
#define FFT_SEQ_HPP
#include "FFTProcess.hpp"
#include "fftbasic.hpp"
#include <complex>

#define TASK_SEQ_LIMIT 32768

class FFTSeqWorker {
public:
	/**Constructor for FFTSeqWorker
	 * @param[n] problem size
	 * @param[wn] unity
	 * @param[sIn] index pointer for input
	 * @param[sOut] index pointer for output*/		
	FFTSeqWorker(
		int n,
		std::complex<double> wn,
		const std::complex<double> *pIn,
		int sIn,
		std::complex<double> *pOut,
		int sOut
	) : n(n), wn(wn), pIn(pIn), sIn(sIn), pOut(pOut), sOut(sOut) {}


	/**Execute function inherited from tbb::task
	 **/
	void execute(void){
		if (n == 1){
			pOut[0] = pIn[0];
	    }else if (n == 2){
			pOut[0] = pIn[0]+pIn[sIn];
			pOut[sOut] = pIn[0]-pIn[sIn];
	    }else{

			unsigned m = n>>1;

			if (n >= TASK_SEQ_LIMIT) {
				FFTSeqWorker(m,wn*wn,pIn,2*sIn,pOut,sOut).execute();
				FFTSeqWorker(m,wn*wn,pIn+sIn,2*sIn,pOut+sOut*m,sOut).execute();
			}else {
				fftbasic::fft_seq(m,wn*wn,pIn,2*sIn,pOut,sOut);
				fftbasic::fft_seq(m,wn*wn,pIn+sIn,2*sIn,pOut+sOut*m,sOut);
			}

			std::complex<double> w=std::complex<double>(1.0, 0.0);
			for (unsigned j=0;j<m;++j){
				std::complex<double> t1 = w*pOut[m+j];
				std::complex<double> t2 = pOut[j]-t1;
				pOut[j] = pOut[j]+t1;               /*  pOut[j] = pOut[j] + w^i pOut[m+j] */
				pOut[j+m] = t2;                  	/*  pOut[j] = pOut[j] - w^i pOut[m+j] */
				w = w*wn;
			}
		}
	}

private:
	int n;
	std::complex<double> wn;
	const std::complex<double> *pIn;
	int sIn;
	std::complex<double> *pOut;
	int sOut;
};

class FFTSeq : public FFTProcess {
public:
	/**Constructor for the FFTSeq class
	 * @param[n] problem size
	 * @param[pIn] pointer to input data
	 * @param[pOut] pointer to output
	 **/	
	FFTSeq(
		int n,
		const std::complex<double> *pIn,
		std::complex<double> *pOut
	) : FFTProcess(n, pIn, pOut) {}

	void run(void){
        const double pi2=6.283185307179586476925286766559;
        double angle = pi2/n;
        std::complex<double> wn(cos(angle), sin(angle));
		FFTSeqWorker *rootWorker = new FFTSeqWorker(n, wn, pIn, 1, pOut, 1);
		rootWorker->execute();
 		delete rootWorker;
	}
};

void fft_seq(int n, const std::complex<double> *pIn, std::complex<double> *pOut) {
	FFTSeq fftSeq(n,pIn,pOut);
	fftSeq.run();
}


#endif
