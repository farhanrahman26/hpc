#ifndef FFT_TBB_H
#define FFT_TBB_H

#include "FFTProcess.hpp"
#include <complex>
#include <tbb/task.h>

class FFTWorker : public tbb::task {
public:
	/**Constructor for FFTWorker
	 * @param[n] problem size
	 * @param[wn] unity
	 * @param[sIn] index pointer for input
	 * @param[sOut] index pointer for output*/
	FFTWorker(
		int n,
		std::complex<double> wn,
		const std::complex<double> *pIn,
		int sIn,
		std::complex<double> *pOut,
		int sOut
	) : n(n), wn(wn), pIn(pIn), sIn(sIn), pOut(pOut), sOut(sOut) {}

	/**Execute function inherited from tbb::task
	 **/
	tbb::task * execute(void){
		if (n == 1){
			pOut[0] = pIn[0];
		}else if (n == 2){
			pOut[0] = pIn[0]+pIn[sIn];
			pOut[sOut] = pIn[0]-pIn[sIn];
		}else{
			unsigned m = n/2;

			tbb::task_list tList;
			
			/*Push two child nodes into the task list*/
			tList.push_back(*new (tbb::task::allocate_child()) FFTWorker(m,wn*wn,pIn,2*sIn,pOut,sOut));
			tList.push_back(*new (tbb::task::allocate_child()) FFTWorker(m,wn*wn,pIn+sIn,2*sIn,pOut+sOut*m,sOut));
		
			/*Spawn and wait*/
			set_ref_count(3);
			tbb::task::spawn_and_wait_for_all(tList);
			
			std::complex<double> w=std::complex<double>(1.0, 0.0);

			/*Do the n point FFT*/
			for (unsigned j=0;j<m;++j){
			  std::complex<double> t1 = w*pOut[m+j];
			  std::complex<double> t2 = pOut[j]-t1;
			  pOut[j] = pOut[j]+t1;             /*  pOut[j] = pOut[j] + w^i pOut[m+j] */
			  pOut[j+m] = t2;                  	/*  pOut[j] = pOut[j] - w^i pOut[m+j] */
			  w = w*wn;
			}
		}
		return NULL;		
	}

private:
	int n;
	std::complex<double> wn;
	const std::complex<double> *pIn;
	int sIn;
	std::complex<double> *pOut;
	int sOut;
};


class FFTTbb : public FFTProcess {
public:
	/**Constructor for the FFTTbb class
	 * @param[n] problem size
	 * @param[pIn] pointer to input data
	 * @param[pOut] pointer to output
	 **/
	FFTTbb(
		int n,
		const std::complex<double> *pIn,
		std::complex<double> *pOut
	) : FFTProcess(n, pIn, pOut) {}

	/**Run method
	 * Responsible for spawning the root task
	 */
	void run(void){
        const double pi2=6.283185307179586476925286766559;
        double angle = pi2/n;
        std::complex<double> wn(cos(angle), sin(angle));
		FFTWorker &rootWorker = *new (tbb::task::allocate_root()) FFTWorker(n, wn, pIn, 1, pOut, 1);
 		tbb::task::spawn_root_and_wait(rootWorker);
	}
};

void fft_tbb(int n, const std::complex<double> *pIn, std::complex<double> *pOut) {
	FFTTbb fftTbb(n,pIn,pOut);
	fftTbb.run();
}

#endif
