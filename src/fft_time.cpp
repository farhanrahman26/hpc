#include <iostream>
#include <cstdlib>
#include <stdio.h>

#include "util.hpp"
#include "Runner.hpp"
#include "Process.hpp"
#include "FFTRegular.hpp"
#include "fft_tbb.hpp"
#include "fft_opt.hpp"
#include "fft_seq.hpp"
#include "CoreInfo.hpp"
#include <tbb/task_scheduler_init.h>

int main(int argc, char * argv[]){
	srand(0);
	
	/*Default parameters*/
	unsigned int pSize = 2;
	int proc = 1;

	/*Command line argument checks*/
	if (argc == 2) {
		pSize = 1 << util::toInt(argv[1]);
		proc = 1;
	} else if (argc == 3) {
		pSize = 1 << util::toInt(argv[1]);
		proc = util::toInt(argv[2]);
	}else {
		std::cerr 
		<< "Please specify the log of the problem size and optionally the number of processors to run on (-1 for automatic selection)"
		<< std::endl;
		exit(1);
	}

	CoreInfo::setCores(proc);
	tbb::task_scheduler_init init(proc);
	/*Generate data*/
	std::vector<std::complex<double> > In(pSize,0.0), Out(pSize);

    std::vector<std::complex<double> > aIn(pSize, 0.0), bIn(pSize, 0.0), cIn(pSize, 0.0), dIn(pSize, 0.0);

	std::vector<std::complex<double> > aOut(pSize), bOut(pSize), cOut(pSize), dOut(pSize);
        
    /*Copy the data through into the inputs*/
	for(int j=0;j<pSize;++j){
        In[j]=std::complex<double>(rand()/(double)(RAND_MAX) - 0.5, rand()/(double)(RAND_MAX) - 0.5);
		aIn[j] = In[j];
		bIn[j] = In[j];
		cIn[j] = In[j];
		dIn[j] = In[j];
    }

    /*Create the processes to be tested*/
	Process *benchMark = new FFTRegular(pSize, &In[0], &Out[0]);

	Process *fftRegular = new FFTRegular(pSize, &aIn[0], &aOut[0]);

	Process *fftTbb = new FFTTbb(pSize, &bIn[0], &bOut[0]);

	Process *fftOpt = new FFTOpt(pSize, &cIn[0], &cOut[0]);

	Process *fftSeq = new FFTSeq(pSize, &dIn[0], &dOut[0]);

	/*Push the processes into a vector*/
	std::vector<Process *> processes;
	processes.push_back(fftRegular);
	processes.push_back(fftTbb);
	processes.push_back(fftOpt);
	processes.push_back(fftSeq);

	/*Create a Runner*/
	Runner r (benchMark, processes);

	/*Run and test processes*/
	r.runProcesses();

	delete benchMark;
	delete fftRegular;
	delete fftOpt;
	delete fftTbb;
	delete fftSeq;

	return 0;
}
