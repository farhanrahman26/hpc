#ifndef FFT_BASIC
#define FFT_BASIC
#include <complex>

namespace fftbasic{
void fft_seq(
	int n,
	std::complex<double> wn,
	const std::complex<double> *pIn,
	int sIn,
	std::complex<double> *pOut,
	int sOut
){
	if (n == 1){
		pOut[0] = pIn[0];
	}else if (n == 2){
		pOut[0] = pIn[0]+pIn[sIn];
		pOut[sOut] = pIn[0]-pIn[sIn];
	}else{
		unsigned m = n>>1;

		fft_seq(m,wn*wn,pIn,2*sIn,pOut,sOut);
		fft_seq(m,wn*wn,pIn+sIn,2*sIn,pOut+sOut*m,sOut);
	
		std::complex<double> w = std::complex<double>(1.0,0.0);
		for (unsigned j=0;j<m;++j){
		  std::complex<double> t1 = w*pOut[m+j];
		  std::complex<double> t2 = pOut[j]-t1;
		  pOut[j] += t1;                 /*  pOut[j] = pOut[j] + w^i pOut[m+j] */
		  pOut[j+m] = t2;                /*  pOut[j+m] = pOut[j] - w^i pOut[m+j] */
		  w = w*wn;
		}
	}

}
}

#endif