#!/usr/bin/env python

import os
import subprocess

def runGD():
        proc = [2, 4]
        probsize = []

        for k in range(0, 24):
                probsize.append(pow(2,k))

        probsize.append(10000000)

        for i in range(0, len(proc)):
                for j in range(0, len(probsize)):
                        arg = "./graph_distance_time " + str(probsize[j]) + " " + str(proc[i])
                        os.system(arg)
                print "\n"


if __name__=="__main__":
        runGD()
