#ifndef GRAPH_DISTANCE_OPT
#define GRAPH_DISTANCE_OPT
#include "GraphDistanceProcess.hpp"
#include <tbb/task.h>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <tbb/concurrent_queue.h>
#include <tbb/mutex.h>
#include <deque>
#include <vector>
#include <utility>

#define AGGPOINT_OPT 24

class GraphOptParObject {
private:
	std::vector<std::pair<int,int> > *todo;
	const std::vector<int> *edges;
	const int secondPlusOne;
	const int wp;
public:
	GraphOptParObject(
		std::vector<std::pair<int,int> > *todo,
		const std::vector<int> *edges,
		const int secondPlusOne,
		const int wp
	) : todo(todo), edges(edges), secondPlusOne(secondPlusOne), wp(wp) {}

	void operator() (tbb::blocked_range<size_t> range) const {
		size_t begin = range.begin();
		size_t end = range.end();
		for(size_t i = begin; i != end; ++i){
			(*todo)[wp+i] = std::make_pair((*edges)[i],secondPlusOne);
		}
	}
};

class GraphDistanceOpt : public GraphDistanceProcess {
public:
	GraphDistanceOpt(
		int start,
		const std::vector<node> *nodes
	) : GraphDistanceProcess(start, nodes) {}

	void run(void){
		// a list of (id,dist)
		delete distance;
		distance = new std::vector<int>((*nodes).size(), INT_MAX);	
		unsigned size = 0;
		/*Convert the vector into a pseudo queue*/
		std::vector<std::pair<int,int> > todo(this->maxVectorSize());

		/*Update the first element with the start node*/
		todo[0] = std::make_pair(start, 0);

		/*Declare the read and write pointers*/
		unsigned rp = 0;
		unsigned wp = 1;
		/*Keep track of a done counter*/
		unsigned done = 0;
		const unsigned distSize = (*distance).size();
		while(rp < todo.size()){
			/*Get the current node to check*/
			std::pair<int,int> curr = todo[rp++];
			if((*distance)[curr.first]==INT_MAX){
				/*Update the distance data if it does not exist*/
				(*distance)[curr.first]=curr.second;
				/*Increase the done counter and if all the data has
				 *been updated then break from the while loop*/
				if(++done == distSize) break;
				size = (*nodes)[curr.first].edges.size();
				/*Spawn a parallel_for depending on the agglomeration
				 *point*/
				if (size < AGGPOINT_OPT){
					int dist = curr.second + 1;
					for(unsigned i=0;i<size;++i){
						todo[wp+i] = std::make_pair((*nodes)[curr.first].edges[i],dist);
					}
				} else {
					parallel_for(tbb::blocked_range<size_t>(0,size,size/8), 
						GraphOptParObject(&todo, &(*nodes)[curr.first].edges,curr.second+1,wp)
					);
				}
				/*Update the write pointer*/
				wp += size;
			} else {
				wp = rp >= wp ? rp : wp;
			}
		}
	}

	unsigned maxVectorSize(void){
		unsigned size = (*nodes).size();
		for(unsigned i = 0; i < (*nodes).size(); ++i){
			size += (*nodes)[i].edges.size();
		}
		return size;
	}

};


std::vector<int> graph_distance_opt(const std::vector<node> &nodes, int start){
	return GraphDistanceOpt(start, &nodes).getDistance();
}

#endif