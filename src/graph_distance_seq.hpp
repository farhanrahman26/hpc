#ifndef GRAPH_DISTANCE_SEQ
#define GRAPH_DISTANCE_SEQ
#include "GraphDistanceProcess.hpp"
#include <vector>
#include <utility>

class GraphDistanceSeq : public GraphDistanceProcess {
public:
	GraphDistanceSeq(
		int start,
		const std::vector<node> *nodes
	) : GraphDistanceProcess(start, nodes) {}

	void run(void){
		// a list of (id,dist)
		delete distance;
		distance = new std::vector<int>((*nodes).size(), INT_MAX);	
		unsigned size = 0;
		std::vector<std::pair<int,int> > todo(this->maxVectorSize());

		todo[0] = std::make_pair(start, 0);

		unsigned rp = 0;
		unsigned wp = 1;
		unsigned done = 0;
		const unsigned distSize = (*distance).size();
		while(rp < todo.size()){
			std::pair<int,int> curr = todo[rp++];
			if((*distance)[curr.first]==INT_MAX){
				(*distance)[curr.first]=curr.second;
				if(++done == distSize) break;
				size = (*nodes)[curr.first].edges.size();
				int dist = curr.second + 1;
				for(unsigned i=0;i<size;++i){
					todo[wp+i] = std::make_pair((*nodes)[curr.first].edges[i],dist);
				}
				wp += size;
			} else {
				wp = rp >= wp ? rp : wp;
			}
		}
	}

	unsigned maxVectorSize(void){
		unsigned size = (*nodes).size();
		for(unsigned i = 0; i < (*nodes).size(); ++i){
			size += (*nodes)[i].edges.size();
		}
		return size;
	}

};

std::vector<int> graph_distance_seq(const std::vector<node> &nodes, int start){
	return GraphDistanceSeq(start, &nodes).getDistance();
}

#endif