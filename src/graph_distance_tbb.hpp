#ifndef GRAPH_DISTANCE_TBB
#define GRAPH_DISTANCE_TBB
#define AGGPOINT 25
#include "GraphDistanceProcess.hpp"
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <vector>
#include <utility>

/*@class[GraphDistanceParForObject]
 *Class responsible for concurrently updating
 *the pseudo queue*/
class GraphDistanceParForObject {
private:
	std::vector<std::pair<int,int> > *todo;
	const std::vector<int> *edges;
	const int secondPlusOne;
	const int li;
public:
	GraphDistanceParForObject(
		std::vector<std::pair<int,int> > *todo,
		const std::vector<int> *edges,
		const int secondPlusOne,
		const int li
	) : todo(todo), edges(edges), secondPlusOne(secondPlusOne), li(li) {}

	void operator() (tbb::blocked_range<size_t> range) const {
		size_t begin = range.begin();
		size_t end = range.end();

		for(size_t i = begin; i != end; ++i){
			(*todo)[li+i] = std::make_pair((*edges)[i],secondPlusOne);
		}
	}
};

class GraphDistanceTbb : public GraphDistanceProcess {
public:
	GraphDistanceTbb(
		int start,
		const std::vector<node> *nodes
	) : GraphDistanceProcess(start, nodes) {}

	void run(void){
		// a list of (id,dist)
		delete distance;
		distance = new std::vector<int>((*nodes).size(), INT_MAX);	
		unsigned size = 0;
		/*Converted the deque to vector to make a pseudo queue*/
		std::vector<std::pair<int,int> > todo;

		/*Initilise with the starting node*/
		todo.push_back(std::make_pair(start,0));

		unsigned counter = 0;

		/*Loop through while there are still nodes to visit*/
		while(counter < todo.size()){
			/*Get the current node and distance pair.
			 *Increment the read counter*/
			std::pair<int,int> curr = todo[counter++];
			
			/*Check if distance data already exists*/
			if((*distance)[curr.first]==INT_MAX){
				/*Update the distance data just discovered*/
				(*distance)[curr.first]=curr.second;
				size = (*nodes)[curr.first].edges.size();
				/*Get the last index of the current todo list*/
				int li = todo.size();
				/*Resize the todo list*/
				todo.resize(todo.size()+size);
				/*Depending on the agglomeration point,
				 *do the parallel for loop to concurrently
				 *update the data structure*/
				if (size < AGGPOINT){
					for(unsigned i=0;i<size;++i){
						todo[li+i] = std::make_pair((*nodes)[curr.first].edges[i],curr.second+1);
					}
				} else {
					parallel_for(tbb::blocked_range<size_t>(0,size), 
						GraphDistanceParForObject(&todo, &(*nodes)[curr.first].edges,curr.second+1,li)
					);
				}			
			}
		}
	}

};

std::vector<int> graph_distance_tbb(const std::vector<node> &nodes, int start){
	return GraphDistanceTbb(start, &nodes).getDistance();
}

#endif