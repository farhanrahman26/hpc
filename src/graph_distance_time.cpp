#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "graph_distance.hpp"
#include "GraphDistanceReg.hpp"
#include "graph_distance_tbb.hpp"
#include "graph_distance_opt.hpp"
#include "graph_distance_seq.hpp"
#include "Runner.hpp"
#include "Process.hpp"
#include "CoreInfo.hpp"
#include <tbb/task_scheduler_init.h>


void dump_distance(int start, std::vector<int> distance) {
	for(int i=0;i<distance.size();++i){
		fprintf(stdout, "dist(%d->%d) = %d\n", start, i, distance[i]);
	}
}

int main(int argc, char * argv[]){
	int n = 0;
	int cores = -1;
	if(argc==2){
		n = atoi(argv[1]);
	} else if (argc == 3){
		n = atoi(argv[1]);
		cores = atoi(argv[2]);
	} else {
		std::cerr<<"Must specify n at least.\n";
		return 1;
	}

	CoreInfo::setCores(cores);
	tbb::task_scheduler_init init(cores);
	
	std::vector<node> graph=build_graph(n);
	
	int start=rand()%n;

	Process *benchMark = new GraphDistanceReg(start, &graph);
	Process *graphReg = new GraphDistanceReg(start, &graph);
	Process *graphTbb = new GraphDistanceTbb(start, &graph);
	Process *graphOpt = new GraphDistanceOpt(start, &graph);
	Process *graphSeq = new GraphDistanceSeq(start, &graph);

	std::vector<Process *> processes;

	processes.push_back(graphReg);
	processes.push_back(graphTbb);
	processes.push_back(graphOpt);
	processes.push_back(graphSeq);

	Runner runner(benchMark, processes);

	runner.runProcesses();

	delete benchMark;
	delete graphReg;
	delete graphTbb;
	delete graphOpt;
	delete graphSeq;
	
	return 0;
}