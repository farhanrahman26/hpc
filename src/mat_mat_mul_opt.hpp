#ifndef MAT_MAT_MUL_OPT_HPP
#define MAT_MAT_MUL_OPT_HPP

#include "MatProcess.hpp"
#include "mat_t.hpp"
#include <tbb/task.h>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include "CoreInfo.hpp"

#define GRAINSIZE 64
#define BLOCKSIZE GRAINSIZE/4

/*@class[ColumnParObject] function object for iterating
 *through the column of the matrix*/
class ColumnParObject {
private:
	mat_t* dst;
	const mat_t* right;
	unsigned row;
public:
	ColumnParObject(
		mat_t *dst,
		const mat_t* right,
		unsigned row
	) : dst(dst), right(right), row(row) {}

	void operator() (tbb::blocked_range<size_t> range) const{
		size_t begin = range.begin();
		size_t end = range.end();
		/*Accumulate the rsults into the destination index*/
		for (size_t i = begin; i != end; ++i){
			dst->at(row,i) += right->at(row,i);
		}
	}

};

/*@class[RowParObject]
 *Class that loops through the rows of the matrix*/
class RowParObject{
private:
	mat_t* dst;
	const mat_t* right;
public:
	RowParObject(
		mat_t* dst,
		const mat_t* right
	) : dst(dst), right(right) {}

	/*@param[range] range through which the Par object iterates*/
	void operator() (tbb::blocked_range<size_t> range) const {
		size_t begin = range.begin();
		size_t end = range.end();
		for(size_t i = begin; i != end; ++i){
			int parForGrain = dst->cols/CoreInfo::getCores();
			/*Check for grain size depending on the number of known processors*/
			if(parForGrain < 0){
				parallel_for(tbb::blocked_range<size_t>(0,dst->cols), ColumnParObject(dst, right, (unsigned)i));
			} else {
				parallel_for(tbb::blocked_range<size_t>(0,dst->cols, parForGrain), ColumnParObject(dst, right, (unsigned)i));
			}
		}
	}

};


class MatOptWorker : public tbb::task {
public:
	MatOptWorker(
		mat_t dst,
		const mat_t& a,
		const mat_t& b
	) : dst(dst), a(a), b(b) {
		cores = CoreInfo::getCores();
	}

	tbb::task* execute(void){
		/*If the grain size has been reached then do the sequential version
		 *This algorithm makes good use of the cache by performing
		 *matrix multiplication in a blocked range. This makes sure that
		 *while the data is present in the cache, it is used optimally*/
		if((dst.rows<=GRAINSIZE) || (dst.cols<=GRAINSIZE)){
			unsigned blocksize = BLOCKSIZE;
			unsigned size = dst.rows;
			for (unsigned jj = 0; jj < size; jj += blocksize){
				for (unsigned kk = 0; kk < size; kk += blocksize){
			    	for (unsigned i = 0; i < size; ++i){
			      		for (unsigned j = jj; j < std::min(jj+blocksize,size); ++j){
			        		double acc = 0.0;
			        		for (unsigned k = kk; k < std::min(kk+blocksize, size); ++k){
			          			acc += a.at(i,k) * b.at(k,j);
			        		}
			        		dst.at(i,j) += acc;
			      		}
			    	}
			  	}
			}			
		}else{
			tbb::task_list tList;

			local_mat_t right(dst.rows, dst.cols);

			/*Spawn the 8 tasks for accumulating multiplication results into
			 *the 4 quads of the matrix*/
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(dst.quad(0,0), a.quad(0,0), b.quad(0,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(dst.quad(0,1), a.quad(0,0), b.quad(0,1)));
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(dst.quad(1,0), a.quad(1,0), b.quad(0,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(dst.quad(1,1), a.quad(1,0), b.quad(0,1)));
	
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(right.quad(0,0), a.quad(0,1), b.quad(1,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(right.quad(0,1), a.quad(0,1), b.quad(1,1)));
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(right.quad(1,0), a.quad(1,1), b.quad(1,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatOptWorker(right.quad(1,1), a.quad(1,1), b.quad(1,1)));

			set_ref_count(9);
	
			tbb::task::spawn_and_wait_for_all(tList);

			int parForGrain = dst.rows/cores;

			/*Check for parallel for grain size depending on the number of processors*/
			if(parForGrain < 0){
				parallel_for(tbb::blocked_range<size_t>(0,dst.rows), RowParObject(&dst, &right));
			} else {
				parallel_for(tbb::blocked_range<size_t>(0,dst.rows, parForGrain), RowParObject(&dst, &right));
			}				

		}
		return NULL;
	}

private:

	mat_t dst;
	const mat_t a;
	const mat_t b;
	int cores;
};

class MatOpt : public MatProcess {
public:

	/*Constructs MatOpt with arguments for multiplication
	 *and a destination to store the multiplication
	 *result.*/
	MatOpt(
		mat_t dst, 
		const mat_t a, 
		const mat_t b
	) : MatProcess(dst,a,b) {}
	

	void run(void){
		unsigned storesize = dst.rows*dst.cols;
		for(unsigned i = 0; i < storesize; ++i){
				dst.mat[i] = 0.0;
		}		
		MatOptWorker &root = *new (tbb::task::allocate_root()) MatOptWorker(dst,a,b);
		tbb::task::spawn_root_and_wait(root);
	}

};

void mat_mat_mul_opt(mat_t dst, const mat_t a, const mat_t b){
		MatOptWorker &root = *new (tbb::task::allocate_root()) MatOptWorker(dst,a,b);
		tbb::task::spawn_root_and_wait(root);
}

#endif
