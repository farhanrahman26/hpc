#ifndef MAT_MAT_MUL_SEQ
#define MAT_MAT_MUL_SEQ

#include "MatProcess.hpp"
#include "mat_t.hpp"

#define GRAINSIZE 64
#define BLOCKSIZE GRAINSIZE/4


class MatSeqWorker {
public:
	MatSeqWorker(
		mat_t dst,
		const mat_t& a,
		const mat_t& b
	) : dst(dst), a(a), b(b) {}

	void execute(void){
		if((dst.rows<=GRAINSIZE) || (dst.cols<=GRAINSIZE)){
			unsigned blocksize = BLOCKSIZE;
			unsigned size = dst.rows;
			for (unsigned jj = 0; jj < size; jj += blocksize){
				for (unsigned kk = 0; kk < size; kk += blocksize){
			    	for (unsigned i = 0; i < size; ++i){
			      		for (unsigned j = jj; j < std::min(jj+blocksize,size); ++j){
			        		double acc = 0.0;
			        		for (unsigned k = kk; k < std::min(kk+blocksize, size); ++k){
			          			acc += a.at(i,k) * b.at(k,j);
			        		}
			        		dst.at(i,j) += acc;
			      		}
			    	}
			  	}
			}			
		}else{
			local_mat_t right(dst.rows, dst.cols);

			MatSeqWorker(dst.quad(0,0), a.quad(0,0), b.quad(0,0)).execute();
			MatSeqWorker(dst.quad(0,1), a.quad(0,0), b.quad(0,1)).execute();
			MatSeqWorker(dst.quad(1,0), a.quad(1,0), b.quad(0,0)).execute();
			MatSeqWorker(dst.quad(1,1), a.quad(1,0), b.quad(0,1)).execute();
	
			MatSeqWorker(right.quad(0,0), a.quad(0,1), b.quad(1,0)).execute();
			MatSeqWorker(right.quad(0,1), a.quad(0,1), b.quad(1,1)).execute();
			MatSeqWorker(right.quad(1,0), a.quad(1,1), b.quad(1,0)).execute();
			MatSeqWorker(right.quad(1,1), a.quad(1,1), b.quad(1,1)).execute();

			for(unsigned row=0;row<dst.rows;++row){
				for(unsigned col=0;col<dst.cols;++col){
					dst.at(row,col) += right.at(row,col);
				}
			}	

		}
	}

private:

	mat_t dst;
	const mat_t a;
	const mat_t b;

};

class MatSeq : public MatProcess {
public:

	/*Constructs MatSeq with arguments for multiplication
	 *and a destination to store the multiplication
	 *result.*/
	MatSeq(
		mat_t dst, 
		const mat_t a, 
		const mat_t b
	) : MatProcess(dst,a,b) {}
	

	void run(void){
		unsigned storesize = dst.rows*dst.cols;
		for(unsigned i = 0; i < storesize; ++i){
				dst.mat[i] = 0.0;
		}		
		MatSeqWorker *root = new MatSeqWorker(dst,a,b);
		root->execute();
		delete root;
	}

};

void mat_mat_mul_seq(mat_t dst, const mat_t a, const mat_t b){
		MatSeqWorker *root = new MatSeqWorker(dst,a,b);
		root->execute();
		delete root;
}

#endif
