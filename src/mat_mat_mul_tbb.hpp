#ifndef MAT_MAT_MUL_TBB_HPP
#define MAT_MAT_MUL_TBB_HPP

#include "MatProcess.hpp"
#include "mat_t.hpp"
#include <tbb/task.h>


class MatWorker : public tbb::task {
public:
	MatWorker(
		mat_t dst,
		const mat_t& a,
		const mat_t& b
	) : dst(dst), a(a), b(b) {}

	tbb::task* execute(void){
		if((dst.rows==1) || (dst.cols==1)){
			for(unsigned row=0;row<dst.rows;++row){
				for(unsigned col=0;col<dst.cols;++col){
					double acc=0.0;
					for(unsigned i=0;i<a.cols;++i){
						acc += a.at(row,i) * b.at(i,col);
					}
					dst.at(row,col) = acc;
				}
			}
		}else{
			tbb::task_list tList;

			local_mat_t right(dst.rows, dst.cols);

			/*Spawn 8 nodes for the 4 quads to be updated*/
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(dst.quad(0,0), a.quad(0,0), b.quad(0,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(dst.quad(0,1), a.quad(0,0), b.quad(0,1)));
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(dst.quad(1,0), a.quad(1,0), b.quad(0,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(dst.quad(1,1), a.quad(1,0), b.quad(0,1)));
	
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(right.quad(0,0), a.quad(0,1), b.quad(1,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(right.quad(0,1), a.quad(0,1), b.quad(1,1)));
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(right.quad(1,0), a.quad(1,1), b.quad(1,0)));
			tList.push_back(*new (tbb::task::allocate_child()) MatWorker(right.quad(1,1), a.quad(1,1), b.quad(1,1)));

			set_ref_count(9);
	
			tbb::task::spawn_and_wait_for_all(tList);

			/*Accumulate the results into the dst*/
			for(unsigned row=0;row<dst.rows;++row){
				for(unsigned col=0;col<dst.cols;++col){
					dst.at(row,col) += right.at(row,col);
				}
			}
		}
		return NULL;
	}

private:

	mat_t dst;
	const mat_t a;
	const mat_t b;

};

class MatTbb : public MatProcess {
public:

	/*Constructs MatTbb with arguments for multiplication
	 *and a destination to store the multiplication
	 *result.*/
	MatTbb(
		mat_t dst, 
		const mat_t a, 
		const mat_t b
	) : MatProcess(dst,a,b) {}
	

	void run(void){
		MatWorker &root = *new (tbb::task::allocate_root()) MatWorker(dst,a,b);
		tbb::task::spawn_root_and_wait(root);
	}

};

void mat_mat_mul_tbb(mat_t dst, const mat_t a, const mat_t b){
		MatWorker &root = *new (tbb::task::allocate_root()) MatWorker(dst,a,b);
		tbb::task::spawn_root_and_wait(root);
}

#endif
