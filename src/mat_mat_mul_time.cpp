#include <iostream>
#include <vector>
#include "Runner.hpp"
#include "MatMatRegular.hpp"
#include "mat_mat_mul.hpp"
#include "mat_mat_mul_tbb.hpp"
#include "mat_mat_mul_opt.hpp"
#include "mat_mat_mul_seq.hpp"
#include "mat_t.hpp"
#include "CoreInfo.hpp"

std::ostream& operator<<(std::ostream &oss, const mat_t& matrix){
	unsigned rows = matrix.rows;
	unsigned cols = matrix.cols;

	for(unsigned r=0;r<rows;++r){
		oss<<"[";
		for(unsigned c=0;c<cols;++c){
			if(c!=0)
				oss<<" , ";
			oss<< matrix.at(r,c);
		}
		oss<<"]\n";
	}
	oss<<"\n";
	return oss;
}


int main(int argc, char * argv[]){

	srand(0);
	
	int n=8;
	int cores = -1;
	if(argc>1)
		n=atoi(argv[1]);

	if(argc==2){
		n = atoi(argv[1]);
	} else if (argc == 3){
		n = atoi(argv[1]);
		cores = atoi(argv[2]);
	} else {
		std::cerr<<"Must specify n at least.\n";
		return 1;
	}

	CoreInfo::setCores(cores);
	tbb::task_scheduler_init init(cores);

	/*Input and output data for
	 *benchmark process*/	
	local_mat_t A(n,n), B(n,n);
	local_mat_t R(n,n);

	/*Output for the test processes*/
	local_mat_t aR(n,n), bR(n,n), cR(n,n), dR(n,n);

	/*Randomise the data*/
	A.randomise();
	B.randomise();

	/*Test processes*/
	Process *matReg = new MatMatRegular(aR,A,B);
	Process *matTbb = new MatTbb(bR,A,B);
	Process *matOpt = new MatOpt(cR,A,B);
	Process *matSeq = new MatSeq(dR,A,B);

	/*Benchmark process*/
	Process *benchMark = new MatMatRegular(R,A,B);

	std::vector<Process *> processes;

	processes.push_back(matReg);
	processes.push_back(matTbb);
	processes.push_back(matOpt);
	processes.push_back(matSeq);

	Runner runner(benchMark, processes);

	runner.runProcesses();

	delete matReg;
	delete matTbb;
	delete matOpt;
	delete matSeq;
	delete benchMark;

	return 0;
}
