#!/usr/bin/env python

import os
import subprocess

def runMM():
        proc = [2,4]
        probsize = [4,8,16,32,64,128,256,512]
        for i in range(0, len(proc)):
                for j in range(0, len(probsize)):
                        arg = "./mat_mat_mul_time " + str(probsize[j]) + " " + str(proc[i])
                        os.system(arg)
                print "\n"


if __name__=="__main__":
        runMM()
