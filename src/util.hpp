#ifndef UTIL_H
#define UTIL_H

#include <sstream>
#include <string>

namespace util{

template<typename T>
std::string toString(T t){
	std::stringstream ss;
	std::string ret;
	ss << t;
	ss >> ret;
	return ret;
}

template<typename T>
int toInt(T t){
	std::stringstream ss;
	int ret;
	ss << t;
	ss >> ret;
	return ret;
}
}

#endif
